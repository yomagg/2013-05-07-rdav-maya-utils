#!/bin/bash
# The protocol file generated by protogen.py ver. 1.0 on 2013-06-05 21:11
# AUTHOR: Magda S aka Magic Magg magg dot gatech at gmail dot com
# DATE: 2013-06-05
# PURPOSE: 
# REQUIREMENTS:
VERSION="1.0"

# bad arguments
readonly E_BADARGS=65

if [ $# -eq 0 ]; then
	echo "Usage: `basename $0` expr-prefix sim-type walltime chkpt-to-lsf core_no_1 core_no_2 core_no_3 ..."
	echo "Description: sets the series of experiments for the specified number of cores"
	echo "  expr-prefix the prefix which will be the prefix of the experiments"
	echo "  sim-type The type of the simulation: nsbh or r1"
	echo "  walltime How long the simulation should run"
	echo "  chkpt-to-lsf Which checkpoint will be lsf"
	echo "  core_no_1 how many cores for the first experiment"
	echo "  core_no_2 how many cores for the second experiment"
	echo
	echo "E.g.: `basename $0` e0 nsbh 02:00:00 16 192 384 768"
	echo "E.g.: `basename $0` e0 r1 00:40:00 128 48 96 192"
	exit $E_BADARGS
fi

EXPR_PFX=$1
SIM_TYPE=$2
WALLTIME=$3
CHKPT_TO_LSF=$4

# remove first unimportant arguments
shift
shift
shift
shift

dirnames=""

for var in "$@"
do
	./set_expr.sh $EXPR_PFX-$SIM_TYPE-hdf5-$var $var 4 $WALLTIME $SIM_TYPE-hdf5.par	$CHKPT_TO_LSF
	./set_expr.sh $EXPR_PFX-$SIM_TYPE-adios-$var $var 4 $WALLTIME $SIM_TYPE-adios.par $CHKPT_TO_LSF
	
	# this will be  required for
	dirnames+=" "
	dirnames+="$EXPR_PFX-$SIM_TYPE-hdf5-$var"
	dirnames+=" "
	dirnames+="$EXPR_PFX-$SIM_TYPE-adios-$var"
done 

QSUB_NAME=$EXPR_PFX-$SIM_TYPE-qsub-all.sh

cat > $QSUB_NAME <<qsub-sh
#!/bin/sh
# File generated by $(basename $0) on $(date)

dirname=($dirnames )

for var in \${dirname[@]}; do
	cd \$var
	echo "\$var: SUBMITTING maya.pbs ..."
	qsub maya.pbs
	cd ..
	sleep 2
done

qsub-sh

chmod 755 $QSUB_NAME

if [ ! -f readme.txt ]; then
    # file does not exist, so create it
	cat > readme.txt <<readme-file
# File readme.txt created by $(basename $0) ver. $VERSION by Magda S. aka Magic Magg
# File created on $(date "+%F %T") 

# EOF
readme-file

fi

# EOF
