
ActiveThorns = "Kranc2BSSNChi TwoPuncturesObject GenericFD CoordBase SymBase Boundary NanChecker CartGrid3d Time MoL CarpetIOBasic CarpetIOScalar IOUtil Vectors Carpet CarpetLib CarpetReduce CarpetInterp CarpetSlab  CarpetIOASCII ADMBase  StaticConformal  SphericalSurface  AEILocalInterp SpaceMask  Slab CarpetIOADIOS  CarpetRegrid2 CarpetTracker Ylm_Decomp WeylScal4 ShiftTracker MinTracker TimerReport RunStats Dissipation InitBase RotatingSymmetry180 ReflectionSymmetry IHSpin SphereRad SphereIntegrate LoopControl GSL Formaline ADMConstraints ADMCoupling ADMMacros ADIOS"

#############################################################
# Grid
#############################################################

CartGrid3D::type                        = "coordbase"
CartGrid3D::domain                      = "full"
CartGrid3D::avoid_origin                = "no"

CoordBase::domainsize                   = minmax
CoordBase::xmin                         = 0
CoordBase::ymin                         = -317.44
CoordBase::zmin                         = 0
CoordBase::xmax                         = 317.44
CoordBase::ymax                         = 317.44
CoordBase::zmax                         = 317.44
CoordBase::dx                           = 4.9599999999999999645
CoordBase::dy                           = 4.9599999999999999645
CoordBase::dz                           = 4.9599999999999999645
CoordBase::boundary_size_x_lower        = 3
CoordBase::boundary_size_y_lower        = 3
CoordBase::boundary_size_z_lower        = 3
CoordBase::boundary_shiftout_x_lower    = 1
CoordBase::boundary_shiftout_y_lower    = 0
CoordBase::boundary_shiftout_z_lower    = 1
CoordBase::boundary_size_x_upper        = 3
CoordBase::boundary_size_y_upper        = 3
CoordBase::boundary_size_z_upper        = 3
CoordBase::boundary_shiftout_x_upper    = 0
CoordBase::boundary_shiftout_y_upper    = 0
CoordBase::boundary_shiftout_z_upper    = 0

#############################################################
# Symmetries
#############################################################

ReflectionSymmetry::reflection_x        = "no"
ReflectionSymmetry::reflection_y        = "no"
ReflectionSymmetry::reflection_z        = "yes"
ReflectionSymmetry::avoid_origin_x      = "no"
ReflectionSymmetry::avoid_origin_y      = "no"
ReflectionSymmetry::avoid_origin_z      = "no"

 RotatingSymmetry180::poison_boundaries  = "yes"

#############################################################
# Run statistics
#############################################################

RunStats::calc_every         = 64
RunStats::output_mem_every   = 64
TimerReport::out_every       = 64
TimerReport::out_filename    = "TimerReport"

#############################################################
# CarpetRegrid
#############################################################


#############################################################
# CarpetRegrid2/CarpetTracker
#############################################################

CarpetRegrid2::num_centres     = 3
CarpetRegrid2::regrid_every          = 64
CarpetRegrid2::freeze_unaligned_parent_levels = "yes"
CarpetRegrid2::freeze_unaligned_levels = "yes"
CarpetRegrid2::verbose               = 0
CarpetRegrid2::symmetry_rotating180   = "yes"
CarpetRegrid2::snap_to_coarse = "no"
CarpetRegrid2::min_distance = 0
CarpetRegrid2::min_fraction = 0.4

CarpetRegrid2::num_levels_1  =  9
CarpetTracker::surface     [0] = 0
CarpetRegrid2::position_x_1  =  3.257
CarpetRegrid2::position_y_1  =  0
CarpetRegrid2::position_z_1  =  0

CarpetRegrid2::radius_1[1]  =  158.72
CarpetRegrid2::radius_1[2]  =  79.36
CarpetRegrid2::radius_1[3]  =  39.68
CarpetRegrid2::radius_1[4]  =  9.92
CarpetRegrid2::radius_1[5]  =  4.96
CarpetRegrid2::radius_1[6]  =  2.48
CarpetRegrid2::radius_1[7]  =  1.24
CarpetRegrid2::radius_1[8]  =  0.62

CarpetRegrid2::num_levels_2  =  9
CarpetTracker::surface     [1] = 1
CarpetRegrid2::position_x_2  =  -3.257
CarpetRegrid2::position_y_2  =  0
CarpetRegrid2::position_z_2  =  0
CarpetRegrid2::radius_2[1]  =  158.72
CarpetRegrid2::radius_2[2]  =  79.36
CarpetRegrid2::radius_2[3]  =  39.68
CarpetRegrid2::radius_2[4]  =  9.92
CarpetRegrid2::radius_2[5]  =  4.96
CarpetRegrid2::radius_2[6]  =  2.48
CarpetRegrid2::radius_2[7]  =  1.24
CarpetRegrid2::radius_2[8]  =  0.62

CarpetRegrid2::num_levels_3  =  6
CarpetRegrid2::position_x_3  =  0
CarpetRegrid2::position_y_3  =  0
CarpetRegrid2::position_z_3  =  0
CarpetRegrid2::radius_3[1]  =  158.72
CarpetRegrid2::radius_3[2]  =  79.36
CarpetRegrid2::radius_3[3]  =  39.68
CarpetRegrid2::radius_3[4]  =  9.92
CarpetRegrid2::radius_3[5]  =  4.96

#############################################################
# SphericalSurface
#############################################################

SphericalSurface::nsurfaces  = 6
SphericalSurface::maxntheta  = 120
SphericalSurface::maxnphi    = 40

#############################################################
# Minimum Tracker
#############################################################

MinTracker::x0[0] = 3.257
MinTracker::y0[0] = 0
MinTracker::z0[0] = 0
MinTracker::surface_index[0] = 0

MinTracker::x0[1] = -3.257
MinTracker::y0[1] = 0
MinTracker::z0[1] = 0
MinTracker::surface_index[1] = 1

MinTracker::nminima = 2
MinTracker::var     = "Kranc2BSSNChi::chi"
MinTracker::verbose = 1
MinTracker::newton_gamma = 1
MinTracker::newton_tolerance = 1e-13
MinTracker::interpolator_name = "Lagrange polynomial interpolation"
MinTracker::interpolator_pars = "order=4"
MinTracker::find_every        = 32

#############################################################
# Shift tracker
#############################################################

ShiftTracker::x0[0]              = 3.257
ShiftTracker::y0[0]              = 0.0
ShiftTracker::z0[0]              = 0.0
#ShiftTracker::surface_index[0]   = 0

ShiftTracker::x0[1]              = -3.257
ShiftTracker::y0[1]              = 0.0
ShiftTracker::z0[1]              = 0.0
#ShiftTracker::surface_index[1]   = 1

ShiftTracker::num_trackers       = 2
ShiftTracker::verbose            = 0
ShiftTracker::output_every       = 1
ShiftTracker::interpolator_name  = "Lagrange polynomial interpolation"
ShiftTracker::interpolator_pars  = "order=4"
ShiftTracker::beta1_var          = "Kranc2BSSNChi::beta1"
ShiftTracker::beta2_var          = "Kranc2BSSNChi::beta2"
ShiftTracker::beta3_var          = "Kranc2BSSNChi::beta3"

#############################################################
# Carpet
#############################################################

Carpet::ghost_size                      = 3
Carpet::domain_from_coordbase           = "yes"
Carpet::max_refinement_levels           = 9
Carpet::refinement_factor               = 2
Carpet::prolongation_order_space        = 5
Carpet::prolongation_order_time         = 2
Carpet::use_buffer_zones                = "yes"
Carpet::verbose                         = "no"
Carpet::time_refinement_factors         = "[1,1,1,2,4,8,16,32,64]"
Carpet::output_timers_every             = 64
Carpet::poison_new_timelevels           = "yes"
Carpet::check_for_poison                = "no"
Carpet::poison_value                    = 113
Carpet::print_timestats_every           = 0
Carpet::init_fill_timelevels            = "yes"

#############################################################
# CarpetLib
#############################################################

CarpetLib::poison_new_memory            = "yes"
CarpetLib::poison_value                 = 114
CarpetLib::print_memstats_every         = 64

#############################################################
# Time integration
#############################################################

Cactus::terminate                     = "any"
Cactus::max_runtime                   = 1410
Cactus::cctk_final_time               = 500
Cactus::cctk_itlast                   = 150
Cactus::cctk_timer_output             = "full"
Cactus::highlight_warning_messages    = "no"

Time::dtfac                           = 0.125

MethodOfLines::ode_method             = "RK4"
MethodOfLines::MoL_NaN_Check          = "no"
MethodOfLines::MoL_Intermediate_Steps = 4
MethodOfLines::MoL_Num_Scratch_Levels = 1

#############################################################
# Initial data
#############################################################

ADMBase::initial_data = "twopuncturesobject"
ADMBase::initial_lapse = "utb"
ADMBase::initial_shift = "zero"

ADMBase::evolution_method = "Kranc2BSSNChi"
ADMBase::lapse_evolution_method = "Kranc2BSSNChi"
ADMBase::shift_evolution_method = "Kranc2BSSNChi"
ADMBase::dtlapse_evolution_method = "Kranc2BSSNChi"
ADMBase::dtshift_evolution_method = "Kranc2BSSNChi"

# Uncomment these for fast but very inaccurate initial data
#       TwoPuncturesObject::npoints_A = 6
#       TwoPuncturesObject::npoints_B = 6
#       TwoPuncturesObject::npoints_phi = 6

TwoPuncturesObject::par_b          =  3.257
TwoPuncturesObject::par_m[0]     =  0.483
TwoPuncturesObject::par_m[1]    =  0.483
TwoPuncturesObject::par_Py[0]  =  0.133
TwoPuncturesObject::par_Py[1] = -0.133
# TwoPuncturesObject::grid_setup_method = "evaluation"
TwoPuncturesObject::epsilon = 0

#############################################################
# Evolution system
#############################################################

Kranc2BSSNChi::m                        = 1
Kranc2BSSNChi::lapseAdvection           = 1
Kranc2BSSNChi::gammaDriverLambda        = 0
Kranc2BSSNChi::betaDotAlphaFactor       = 0.75
Kranc2BSSNChi::etaBeta                  = 2
Kranc2BSSNChi::chiBeta                  = 1
Kranc2BSSNChi::gammaDriverLapsePower    = 0
Kranc2BSSNChi::nasaAdvection            = 1
Kranc2BSSNChi::newNASAAdvection         = 1
Kranc2BSSNChi::betatAdvection           = 1
Kranc2BSSNChi::chiEps                   = 0.0001
Kranc2BSSNChi::verbose                  = 0
Kranc2BSSNChi::fd_order                 = "full4th"
Kranc2BSSNChi::lapse_condition          = "1 + log full4th"
Kranc2BSSNChi::shift_condition          = "NASAfull4th"
Kranc2BSSNChi::boundary_condition       = "radiative"
Kranc2BSSNChi::recalculate_constraints  = "vacuum"

# These specify that no built-in CactusBase/Boundary boundary
# condition is to be applied. These parameters must be present or the
# Cactus symmetry boundary condition will not be applied. 
Kranc2BSSNChi::A_group_bound      = "none"
Kranc2BSSNChi::alpha_group_bound  = "none"
Kranc2BSSNChi::beta_group_bound   = "none"
Kranc2BSSNChi::betat_group_bound  = "none"
Kranc2BSSNChi::Gam_group_bound    = "none"
Kranc2BSSNChi::h_group_bound      = "none"
Kranc2BSSNChi::K_group_bound      = "none"
Kranc2BSSNChi::chi_group_bound    = "none"

#############################################################
# Dissipation
#############################################################

Dissipation::order                 = 5
Dissipation::epsdis                = 0.0
Dissipation::epsdis_for_level[0]         = 0.5
Dissipation::epsdis_for_level[1]         = 0.5
Dissipation::epsdis_for_level[2]         = 0.5
Dissipation::epsdis_for_level[3]         = 0.5
Dissipation::epsdis_for_level[4]         = 0.1
Dissipation::epsdis_for_level[5]         = 0.1
Dissipation::epsdis_for_level[6]         = 0.1
Dissipation::epsdis_for_level[7]         = 0.1
Dissipation::epsdis_for_level[8]         = 0.1
Dissipation::vars                  = "Kranc2BSSNChi::A_group Kranc2BSSNChi::alpha_group Kranc2BSSNChi::beta_group Kranc2BSSNChi::betat_group Kranc2BSSNChi::Gam_group Kranc2BSSNChi::h_group Kranc2BSSNChi::K_group Kranc2BSSNChi::chi_group"

#############################################################
# Wave extraction
#############################################################

Ylm_Decomp::number_of_detectors      = 13
Ylm_Decomp::detector_radius[0]       = 30
Ylm_Decomp::detector_radius[1]       = 40
Ylm_Decomp::detector_radius[2]       = 50
Ylm_Decomp::detector_radius[3]       = 60
Ylm_Decomp::detector_radius[4]       = 70
Ylm_Decomp::detector_radius[5]       = 80
Ylm_Decomp::detector_radius[6]       = 90
Ylm_Decomp::detector_radius[7]       = 100
Ylm_Decomp::detector_radius[8]       = 110
Ylm_Decomp::detector_radius[9]       = 120
Ylm_Decomp::detector_radius[10]      = 130
Ylm_Decomp::detector_radius[11]      = 140
Ylm_Decomp::detector_radius[12]      = 150
Ylm_Decomp::out_every_det[0]         = 32
Ylm_Decomp::out_every_det[1]         = 32
Ylm_Decomp::out_every_det[2]         = 64
Ylm_Decomp::out_every_det[3]         = 64
Ylm_Decomp::out_every_det[4]         = 64
Ylm_Decomp::out_every_det[5]         = 64
Ylm_Decomp::out_every_det[6]         = 128
Ylm_Decomp::out_every_det[7]         = 128
Ylm_Decomp::out_every_det[8]         = 128
Ylm_Decomp::out_every_det[9]         = 128
Ylm_Decomp::out_every_det[10]        = 128
Ylm_Decomp::out_every_det[11]        = 128
Ylm_Decomp::out_every_det[12]        = 128
Ylm_Decomp::gridfunctions            = "WeylScal4::Psi4r{sw=-2 cmplx='WeylScal4::Psi4i'}"
Ylm_Decomp::verbose                  = 0
Ylm_Decomp::l_mode                   = 4
Ylm_Decomp::m_mode                   = 4
Ylm_Decomp::interpolation_operator   = "Lagrange polynomial interpolation"
Ylm_Decomp::interpolation_order      = 4
Ylm_Decomp::ntheta                   = 160
Ylm_Decomp::nphi                     = 320

#WeylScal4::offset                    = 1e-8
WeylScal4::fd_order                  = "4th"
WeylScal4::verbose                   = 0

#############################################################
# IHSpin
#############################################################

IHSpin::compute_every          = 32
IHSpin::num_horizons           = 3
IHSpin::surface_index[0]       = 2
IHSpin::surface_index[1]       = 3
IHSpin::surface_index[2]       = 4
IHSpin::interpolator_name      = "Lagrange polynomial interpolation"
IHSpin::interpolator_pars      = "order=4"
IHSpin::verbose = 0

SphericalSurface::ntheta[2]    = 120
SphericalSurface::nphi[2]      = 40
SphericalSurface::ntheta[3]    = 120
SphericalSurface::nphi[3]      = 40
SphericalSurface::ntheta[4]    = 120
SphericalSurface::nphi[4]      = 40

#############################################################
# SphereRad
#############################################################

SphereRad::max_nsurface           = 3
SphereRad::surface_index[0]       = 2
SphereRad::surface_index[1]       = 3
SphereRad::surface_index[2]       = 4
SphereRad::shifttracker_index[0]  = 0
SphereRad::shifttracker_index[1]  = 1
SphereRad::force_position[2]      = "yes"
SphereRad::radius_surface[0]      = 0.4
SphereRad::radius_surface[1]      = 0.4
SphereRad::radius_surface[2]      = 1
SphereRad::verbose                = 0

#############################################################
# ADMConstraints
#############################################################

ADMConstraints::constraints_persist = "yes"
ADMConstraints::constraints_timelevels = 3

#############################################################
# Output
#############################################################

IO::out_dir                          = $parfile
IO::out_fileinfo                     = "all"

CarpetIOBasic::outInfo_every         = 1
CarpetIOBasic::outInfo_vars          = "Kranc2BSSNChi::alpha RunStats::speed RunStats::maxrss_mb RunStats::fordblks_kb"
CarpetIOBasic::real_max              = 1e6
CarpetIOBasic::int_width             = 12

CarpetIOScalar::outScalar_every      = 0 #128
CarpetIOScalar::outScalar_reductions = "norm2 minimum"
CarpetIOScalar::outScalar_vars       = "Kranc2BSSNChi::scalarconstraints Kranc2BSSNChi::alpha Kranc2BSSNChi::chi ADMConstraints::hamiltonian ADMConstraints::momentum"

CarpetIOASCII::out1D_every           = 0 #128
CarpetIOASCII::out1D_x               = "yes"
CarpetIOASCII::out1D_y               = "no"
CarpetIOASCII::out1D_z               = "no"
CarpetIOASCII::out1D_vars            = "Kranc2BSSNChi::h11 ADMBase::gxx Kranc2BSSNChi::alpha Kranc2BSSNChi::beta1 ADMBase::betax Kranc2BSSNChi::bssnham Kranc2BSSNChi::A11 Kranc2BSSNChi::K Kranc2BSSNChi::chi Kranc2BSSNChi::Gam1 Kranc2BSSNChi::chirhs WeylScal4::Psi4r ADMConstraints::hamiltonian ADMConstraints::momentum"
CarpetIOASCII::out_precision         = 19

CarpetIOADIOS::out2D_every           = 0 #128
CarpetIOADIOS::out2D_vars            = "Kranc2BSSNChi::alpha kranc2bssnchi::chi kranc2bssnchi::K WeylScal4::Psi4r"
CarpetIOADIOS::out2D_xz              = "no"
CarpetIOADIOS::out2D_yz              = "no"

CarpetIOADIOS::out_every              = 0 #32
CarpetIOADIOS::out_vars               = "Kranc2BSSNChi::alpha WeylScal4::Psi4r"

#############################################################
# Checkpoint and recovery
#############################################################

CarpetIOADIOS::checkpoint       = "yes"

CarpetIOADIOS::adios_method          = "MPI"
CarpetIOADIOS::adios_method_params   = ""
CarpetIOADIOS::adios_buffer_size     = 800

IO::checkpoint_every           = 128
IO::checkpoint_keep            = 3
IO::checkpoint_dir             = chkpts
IO::checkpoint_on_terminate    = "yes"

IO::recover                    = "autoprobe"
IO::recover_dir                = chkpts
IO::recover_and_remove         = "no"

