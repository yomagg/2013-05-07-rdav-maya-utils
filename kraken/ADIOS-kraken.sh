#! /bin/bash

################################################################################
# Prepare
################################################################################

# Set up shell
set -x                          # Output commands
set -e                          # Abort on errors



################################################################################
# Check for old mechanism
################################################################################

if [ -n "${ADIOS}" ]; then
    echo 'BEGIN ERROR'
    echo "Setting the option \"ADIOS\" is incompatible with the ADIOS thorn. Please remove the option ADIOS = ${ADIOS}."
    echo 'END ERROR'
    exit 1
fi



################################################################################
# Decide which libraries to link with
################################################################################

# Set up names of the Fortran and C++ libraries based on user settable variables
# Assign default values to variables
if [ "${ADIOS_ENABLE_CXX:=yes}" = 'yes' ]; then
    ADIOS_CXX_LIBS="adios"
fi
if [ "${ADIOS_ENABLE_FORTRAN:=yes}" = 'yes' ]; then
    ADIOS_FORTRAN_LIBS=$(if [ "${F90}" != "none" ]; then echo 'adiosf'; fi)
fi


################################################################################
# Check version
################################################################################

if [ "${ADIOS_DIR}" ]; then
    echo "BEGIN MESSAGE"
    echo "ADIOS_DIR set, checking version..."
    echo "END MESSAGE"

    ADIOS_VER="$(${ADIOS_DIR}/bin/adios_config -v | cut -d" " -f 3)"

    if [ "${ADIOS_VER}" == "1.5.0" -o "${ADIOS_VER}" == "1.5.1" ]; then
        echo "BEGIN MESSAGE"
        echo "Found version, ${ADIOS_VER}. We're good to go."
        echo "END MESSAGE"
    else
        echo "BEGIN MESSAGE"
        echo "Found version, ${ADIOS_VER}. We need 1.5.0+, building."
        echo "END MESSAGE"
        ADIOS_DIR=''
    fi
fi

################################################################################
# Search
################################################################################

if [ -z "${ADIOS_DIR}" ]; then
    echo "BEGIN MESSAGE"
    echo "ADIOS selected, but ADIOS_DIR not set. Checking some places..."
    echo "END MESSAGE"
    
    FILES="include/adios.h lib/libadios.a $(if [ "${F90}" != "none" ]; then echo 'lib/libadiosf.a'; fi) "
    DIRS="/usr /usr/local /usr/local/adios /usr/local/packages/adios /usr/local/apps/adios /opt/local ${HOME} ${HOME}/adios c:/packages/adios"
    for dir in $DIRS; do
        ADIOS_DIR="$dir"
        for file in $FILES; do
            if [ ! -r "$dir/$file" ]; then
                unset ADIOS_DIR
                break
            fi
        done
        if [ -n "$ADIOS_DIR" ]; then
            break
        fi
    done
    
    if [ -z "$ADIOS_DIR" ]; then
        echo "BEGIN MESSAGE"
        echo "ADIOS not found"
        echo "END MESSAGE"
    else
        echo "BEGIN MESSAGE"
        echo "Found ADIOS in ${ADIOS_DIR}, checking version..."
        echo "END MESSAGE"

        ADIOS_VER="$(${ADIOS_DIR}/bin/adios_config -v | cut -d" " -f 3)"

        if [ "${ADIOS_VER}" == "1.5.0" -o "${ADIOS_VER}" == "1.5.1" ]; then
            echo "BEGIN MESSAGE"
            echo "Found version, ${ADIOS_VER}. We're good to go."
            echo "END MESSAGE"
        else
            echo "BEGIN MESSAGE"
            echo "Found version, ${ADIOS_VER}. We need 1.5.0+, building."
            echo "END MESSAGE"
            ADIOS_DIR=''
        fi
    fi
fi



################################################################################
# Build
################################################################################

if [ -z "${ADIOS_DIR}" -o "${ADIOS_DIR}" = 'BUILD' ]; then
    echo "BEGIN MESSAGE"
    echo "Building ADIOS..."
    echo "END MESSAGE"
    
    # Set locations
    THORN=ADIOS
    NAME=adios-1.5.0
    MXMLNAME=mxml-2.6
    SRCDIR=$(dirname $0)
    BUILD_DIR=${SCRATCH_BUILD}/build/${THORN}
    if [ -z "${ADIOS_INSTALL_DIR}" ]; then
        echo "BEGIN MESSAGE"
        echo "ADIOS install directory, ADIOS_INSTALL_DIR, not set. Installing in the default configuration location. "
        echo "END MESSAGE"
     INSTALL_DIR=${SCRATCH_BUILD}/external/${THORN}
    else
        echo "BEGIN MESSAGE"
        echo "ADIOS install directory, ADIOS_INSTALL_DIR, selected. Installing ADIOS at ${ADIOS_INSTALL_DIR} "
        echo "END MESSAGE"
     INSTALL_DIR=${ADIOS_INSTALL_DIR}
    fi
    DONE_FILE=${SCRATCH_BUILD}/done/${THORN}
    ADIOS_DIR=${INSTALL_DIR}
    
(
    exec >&2                    # Redirect stdout to stderr
    set -x                      # Output commands
    set -e                      # Abort on errors
    cd ${SCRATCH_BUILD}
    if [ -e ${DONE_FILE} -a ${DONE_FILE} -nt ${SRCDIR}/dist/${NAME}.tar.gz \
                         -a ${DONE_FILE} -nt ${SRCDIR}/ADIOS.sh ]
    then
        echo "ADIOS: The enclosed ADIOS library has already been built; doing nothing"
    else
        echo "ADIOS: Building enclosed ADIOS library"
        
        # Should we use gmake or make?
        MAKE=$(gmake --help > /dev/null 2>&1 && echo gmake || echo make)
        # Should we use gtar or tar?
        TAR=$(gtar --help > /dev/null 2> /dev/null && echo gtar || echo tar)
        # Should we use gpatch or patch?
        if [ -z "$PATCH" ]; then
            PATCH=$(gpatch -v > /dev/null 2>&1 && echo gpatch || echo patch)
        fi
        
        # Set up environment

        export CC=mpicc
        export CFLAGS="-g"
        export LIBS= 
        export FC=gfortran
        export CXX=mpicxx
        export CXXFLAGS=

        #if [ "${F90}" = "none" ]; then
        #    echo 'BEGIN MESSAGE'
        #    echo 'No Fortran 90 compiler available. Building ADIOS library without Fortran support.'
        #    echo 'END MESSAGE'
        #    unset FC
        #    unset FCFLAGS
        #else
        #    export FC="${F90}"
        #    export FCFLAGS="${F90FLAGS}"
        #fi
        #export LDFLAGS
        #unset LIBS
        #unset RPATH
        #if echo '' ${ARFLAGS} | grep 64 > /dev/null 2>&1; then
        #    export OBJECT_MODE=64
        #fi
        
        echo "ADIOS: Preparing directory structure..."
        mkdir build external done 2> /dev/null || true
        rm -rf ${BUILD_DIR} ${INSTALL_DIR}
        mkdir ${BUILD_DIR} ${INSTALL_DIR}
        
        echo "ADIOS: Unpacking archive..."
        pushd ${BUILD_DIR}
        echo "BUILD_DIR         ${BUILD_DIR}"
        ${TAR} xzf ${SRCDIR}/dist/${NAME}.tar.gz
        ${TAR} xzf ${SRCDIR}/dist/${MXMLNAME}.tar.gz        

        echo "MXML: Configuring..."
        cd ${MXMLNAME}
        # Do not build Fortran API if it has been disabled, or if
        # there is no Fortran 90 compiler.
        # Do not build C++ API if it has been disabled.
        # Do not build shared library executables if we want to link
        # statically.
        ./configure --prefix=${ADIOS_DIR} --disable-threads #--with-mpi=${MPI_DIR}
        #./configure --prefix=${ADIOS_DIR} --with-zlib=${ZLIB_DIR} --enable-cxx=${ADIOS_ENABLE_CXX} $(if [ -n "${FC}" ]; then echo '' "--enable-fortran=${ADIOS_ENABLE_FORTRAN}"; fi) $(if echo '' "${LDFLAGS}" | grep -q '[-]static'; then echo '' '--disable-shared --enable-static-exec'; fi)

        echo "MXML: Building..."
        ${MAKE}

        echo "MXML: Installing..."
        ${MAKE} install

        cd ..

        echo "ADIOS: Configuring..."
        cd ${NAME}
        # Do not build Fortran API if it has been disabled, or if
        # there is no Fortran 90 compiler.
        # Do not build C++ API if it has been disabled.
        # Do not build shared library executables if we want to link
        # statically.
        ./configure --prefix=${ADIOS_DIR} --with-mxml=${ADIOS_DIR} --disable-fortran #--with-mpi=${MPI_DIR}
        #./configure --prefix=${ADIOS_DIR} --with-zlib=${ZLIB_DIR} --enable-cxx=${ADIOS_ENABLE_CXX} $(if [ -n "${FC}" ]; then echo '' "--enable-fortran=${ADIOS_ENABLE_FORTRAN}"; fi) $(if echo '' "${LDFLAGS}" | grep -q '[-]static'; then echo '' '--disable-shared --enable-static-exec'; fi)
        
        echo "ADIOS: Building..."
        ${MAKE}
        
        echo "ADIOS: Installing..."
        ${MAKE} install
        popd
        
        echo "ADIOS: Cleaning up..."
        rm -rf ${BUILD_DIR}
        
        date > ${DONE_FILE}
        echo "ADIOS: Done."
    fi
)

    if (( $? )); then
        echo 'BEGIN ERROR'
        echo 'Error while building ADIOS. Aborting.'
        echo 'END ERROR'
        exit 1
    fi
    
fi



################################################################################
# Check for additional libraries
################################################################################

# Set options
if [ "${ADIOS_DIR}" = '/usr' -o "${ADIOS_DIR}" = '/usr/local' ]; then
    ADIOS_INC_DIRS=''
    ADIOS_LIB_DIRS=''
else
    # Fortran modules may be located in the lib directory
    ADIOS_INC_DIRS="${ADIOS_DIR}/include ${ADIOS_DIR}/lib /nics/d/home/smagg/opt/mxml-2.7/kraken/gnu/include /nics/d/home/smagg/opt/evpath/kraken/gnu/include"
    #ADIOS_INC_DIRS='/sw/xt/adios/1.3.2_r1552/cnl3.1_pgi11.9.0/include /sw/xt-cle3.1/mxml/2.6/cnl3.1_pgi11.6.0/include /opt/cray/netcdf-hdf5parallel/4.1.1.0/netcdf-hdf5parallel-pgi/include /opt/cray/hdf5-parallel/1.8.5.0/hdf5-parallel-pgi/include'
    #ADIOS_INC_DIRS='/home/matt/software/adios/include /home/matt/software/mxml/include'
    ADIOS_LIB_DIRS="${ADIOS_DIR}/lib /nics/d/home/smagg/opt/mxml-2.7/kraken/gnu/include /nics/d/home/smagg/opt/evpath/kraken/gnu/include" # ${MXML_DIR}/lib"
    #ADIOS_LIB_DIRS='/sw/xt/adios/1.3.2_r1552/cnl3.1_pgi11.9.0/lib /sw/xt-cle3.1/mxml/2.6/cnl3.1_pgi11.6.0/lib /opt/cray/netcdf-hdf5parallel/4.1.1.0/netcdf-hdf5parallel-pgi/lib /opt/cray/hdf5-parallel/1.8.5.0/hdf5-parallel-pgi/lib'
    #ADIOS_LIB_DIRS='/home/matt/software/adios/lib /home/matt/software/mxml/lib'
fi
#ADIOS_LIBS=`${ADIOS_DIR}/bin/adios_config -l`
ADIOS_LIBS='adios mxml m'
# pthread at the end as this is how linker works; remember the order is important
ADIOS_LIBS='adios mxml m evpath ffs atl dill gen_thread cercs_env pthread'


# Check whether we are running on Windows
if perl -we 'exit (`uname` =~ /^CYGWIN/)'; then
    is_windows=0
else
    is_windows=1
fi

# Check whether we are running on MacOS
if perl -we 'exit (`uname` =~ /^Darwin/)'; then
    is_macos=0
else
    is_macos=1
fi




################################################################################
# Configure Cactus
################################################################################

# Pass options to Cactus
echo "BEGIN MAKE_DEFINITION"
echo "HAVE_ADIOS           = 1"
echo "ADIOS_DIR            = ${ADIOS_DIR}"
echo "ADIOS_ENABLE_CXX     = ${ADIOS_ENABLE_CXX}"
echo "ADIOS_ENABLE_FORTRAN = ${ADIOS_ENABLE_FORTRAN}"
echo "ADIOS_INC_DIRS       = ${ADIOS_INC_DIRS}"
echo "ADIOS_LIB_DIRS       = ${ADIOS_LIB_DIRS}"
echo "ADIOS_LIBS           = ${ADIOS_LIBS}"
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(ADIOS_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(ADIOS_LIB_DIRS)'
echo 'LIBRARY           $(ADIOS_LIBS)'
