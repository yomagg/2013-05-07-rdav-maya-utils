# @file readme.txt
# @date: Jun 4, 2013
# @author: Magda S., aka Magic Magg, magg dot gatech at gmail dot com
#
#

bench/*.rpar files are the files from t-square.gatech.edu
R1-adios.rpar is the file I was testing initially.

If you need to have .par files:
$ chmod 755 my.rpar
$ perl my.rpar
$ ls
my.par

If you are using $parfile in your .rpar, escape it properly even
if this is in a comment. The proper escaping is \$parfile 