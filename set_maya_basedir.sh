#!/bin/bash
# The protocol file generated by protogen.py ver. 1.0 on 2013-05-07 13:59
# AUTHOR: Magda S aka Magic Magg magg dot gatech at gmail dot com
# DATE: 2013-05-07
# PURPOSE: 
# REQUIREMENTS:
VERSION="1.0"

# bad arguments
readonly E_BADARGS=65

if [ $# -eq 0 ]; then
	echo "Usage: `basename $0` maya_sourcebasedir thornlist maya_utils"
	echo "Description: creates a maya installation directory; "
	echo "             clones the thorns; downloads GetComponents"
	echo "             creates the get_components for the particular maya_sourcebasedir"
	echo "             copies the R1-adios.rpar file to the parfile script"
	echo
	echo "The script exits if the maya_sourcebasedir exists"
	echo "Requires git be present on the path"
	echo
	echo "Arguments:"
	echo "  maya_sourcebasedir - the sourcebasedir of maya"
	echo "  thornlist - the list of thorns used for this source tree"
	echo "  maya_utils - where the maya utilities are stored"
	echo
	echo "It is suggested that the script will be run only once as it patches"
	echo "some files so I do not know what will happen if I patch the already patched file."
	echo
	echo "After running this script, the machine specific script should be run e.g."
	echo "kraken.sh or nautilus.sh"
	echo "E.g.: `basename $0` 2013-05-03-mayaVac-rdav mayaVac-rdav.th /nics/d/home/smagg/proj/wksp-ecl-cpp/2013-05-07-rdav-maya-utils"
	exit $E_BADARGS
fi

#set -x                          # Output commands
set -e                          # Abort on errors

MAYA_BASEDIR=$1
THORN_LIST=$2

# the directory where patches or other files are located
MAYA_UTILS=$3


if [ -d "$MAYA_BASEDIR" ]; then
    echo "The directory $MAYA_BASEDIR exists. Quitting ..."
    exit 0
fi


# get the thorns lists
git clone -b ET_2012_05 https://www.numrel.org/git/repos/thornlists $MAYA_BASEDIR
cd $MAYA_BASEDIR

# copy the thorn list from Michael to run experiments
cp $MAYA_UTILS/nautilus/expr/config/maya_tpo_rdav.th .
cp $MAYA_UTILS/kraken/expr/config/tpo_mhd_adios.th .

# get the GetComponents
curl -O  https://raw.github.com/gridaphobe/CRL/master/GetComponents
chmod 755 GetComponents

# in case you want to modify the thorn_list
# for r1-hdf5.rpar, nsbh_hdf5 nsbh_adios. might require TwoPuncturesObject
# it is in GTThorns/TwoPuncturesObject
#cp $THORN_LIST $THORN_LIST.org
#vi $THORN_LIST

# create a smart get_components.sh wrapper; you will need to provide the
# correct user name and a password

# -----------------------------------------
# get components
# -----------------------------------------
cat > get_components.sh <<start-of-get_components-file
#!/usr/bin/expect
set timeout 100

#spawn ./GetComponents --parallel --root=Maya-RDAV thornlists/maya-rdav.th
spawn ./GetComponents  --root=Maya-RDAV $THORN_LIST

while {1} {
  expect {
 
    eof                          {break}
    "Do you"                     {send "yes\r"}
    Username*                    {send "magg\r"}
    Password*                    {send "mypassword\r"}
  }
}
#wait -i \$spawn_id
#close \$spawn_id
# eof

start-of-get_components-file

chmod 755 get_components.sh

echo "get_components.sh CREATED ... Modify to provide correct values for the user and the password"

vi get_components.sh

# run the get components
./get_components.sh

# clean the password
sed -i "s;Passwor.*;Password*                    {send \"mypassword\\\r\"};" get_components.sh

# ---------------------------------
# apply a patch
# ---------------------------------
# change to the directory we will be applying patch
#pushd Maya-RDAV/repos/ADIOS/Carpet/CarpetIOADIOS/src
#echo "APPLYING AN IORIGIN PATCH ..."
#cp $MAYA_UTILS/adios_iorigin.patch .
#patch Output.cc < adios_iorigin.patch
## restore the directory
#popd

# --------------------
# copy the R1-adios.par file that can be run with mayaVac-rdav.th list
# --------------------
cp $MAYA_UTILS/parfiles/R1-adios.rpar Maya-RDAV/repos/parfiles/Vacuum_BBH/
# copy the benchmarking rpar files
cp -R $MAYA_UTILS/parfiles/bench Maya-RDAV/repos/parfiles/


echo "==========================================="
echo "Now, run the machine specific script such as kraken.sh, nautilus.sh, heffalin.sh"
echo "Be sure you are on the right branch with repos/ADIOS (timers as of 2013-09-09)
echo "DONE."
echo "==========================================="


# EOF